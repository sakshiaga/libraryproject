import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import SignupForm from './SignupForm';
import LoginForm from './LoginForm';
import Dashboard from './Dashboard';
import AllBooks from './AllBooks';
import MyBooks from './MyBooks';

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<SignupForm />} />

          <Route path="/login" element={<LoginForm />} />

          <Route path="/dashboard" element={<Dashboard />}>
            <Route path="/dashboard/my-books" element={<MyBooks />} />
            <Route path="/dashboard/all-books" element={<AllBooks />} />
          </Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
