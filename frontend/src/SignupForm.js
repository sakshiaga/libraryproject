import React, { useState, useEffect } from "react";
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { TextField, Button, Grid, Typography, Container } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { API_ENDPOINT_BASE_URL } from './Constants';

const SignupForm = () => {
    const navigate = useNavigate();
    const [errorMessage, setErrorMessage] = useState({});

    const formik = useFormik({
        initialValues: {
            name: '',
            phone: '',
            email: '',
            address: '',
            college: '',
            password: '',
            confirmPassword: '',
        },
        validationSchema: Yup.object({
            name: Yup.string().required('Name is required'),
            phone: Yup.string().required('Phone number is required').matches(/^[6-9]\d{9}$/, 'Mobile number is not valid'),
            email: Yup.string().email('Invalid email address').required('Email is required'),
            address: Yup.string().required('Address is required'),
            college: Yup.string().required('College name is required'),
            password: Yup.string().min(8, 'Password must be at least 8 characters').required('Password is required'),
            confirmPassword: Yup.string()
                .required('Please re-type your password')
                .oneOf([Yup.ref('password')], 'Passwords do not match'),
        }),
        onSubmit: (formData) => {
            try {
                const api = axios.create({
                    baseURL: API_ENDPOINT_BASE_URL,
                    headers: {
                        "Content-Type": "application/json",
                    },
                });
                const response = api.post('/api/signup/', formData);
                console.log(response.data);
                navigate('/login');
                setErrorMessage('');
            } catch (error) {
                if (error.response && error.response.data && error.response.data.email) {
                    setErrorMessage('Email address already exists. Please enter a different email.');
                } else {
                    setErrorMessage('An error occurred during signup.');
                }
            }
            console.log('Submitted values:', formData);
        },
    });

    return (
        <Container maxWidth="sm" >
            <form onSubmit={formik.handleSubmit}>
                <Typography variant="h4" gutterBottom>
                    Sign Up
                </Typography>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            id="name"
                            name="name"
                            label="Name"
                            variant="outlined"
                            value={formik.values.name}
                            onChange={formik.handleChange}
                            error={formik.touched.name && Boolean(formik.errors.name)}
                            helperText={formik.touched.name && formik.errors.name}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            id="phone"
                            name="phone"
                            label="Phone"
                            variant="outlined"
                            value={formik.values.phone}
                            onChange={formik.handleChange}
                            error={formik.touched.phone && Boolean(formik.errors.phone)}
                            helperText={formik.touched.phone && formik.errors.phone}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            id="email"
                            name="email"
                            label="Email"
                            variant="outlined"
                            value={formik.values.email}
                            onChange={formik.handleChange}
                            error={formik.touched.email && Boolean(formik.errors.email)}
                            helperText={formik.touched.email && formik.errors.email}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            id="address"
                            name="address"
                            label="Address"
                            variant="outlined"
                            value={formik.values.address}
                            onChange={formik.handleChange}
                            error={formik.touched.address && Boolean(formik.errors.address)}
                            helperText={formik.touched.address && formik.errors.address}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            id="college"
                            name="college"
                            label="College"
                            variant="outlined"
                            value={formik.values.college}
                            onChange={formik.handleChange}
                            error={formik.touched.college && Boolean(formik.errors.college)}
                            helperText={formik.touched.college && formik.errors.college}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            id="password"
                            name="password"
                            label="Password"
                            variant="outlined"
                            type="password"
                            value={formik.values.password}
                            onChange={formik.handleChange}
                            error={formik.touched.password && Boolean(formik.errors.password)}
                            helperText={formik.touched.password && formik.errors.password}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            id="confirmPassword"
                            name="confirmPassword"
                            label="Confirm Password"
                            variant="outlined"
                            type="password"
                            value={formik.values.confirmPassword}
                            onChange={formik.handleChange}
                            error={formik.touched.confirmPassword && Boolean(formik.errors.confirmPassword)}
                            helperText={formik.touched.confirmPassword && formik.errors.confirmPassword}
                        />
                    </Grid>
                </Grid>
                <Button type="submit" variant="contained" color="primary">
                    Sign Up
                </Button>
            </form>
        </Container >
    );
};

export default SignupForm;